/*
 * 系统名称: 
 * 模块名称: webpasser.core
 * 类 名 称: CustomLoadConfig.java
 *   
 */
package com.hxt.webpasser.spider;

import java.util.HashMap;
import java.util.Map;

import com.hxt.webpasser.regular.DecideRule;

/**
 * 功能说明: 自定义配置的加载 <br>
 * 系统版本: v1.0 <br>
 * 作者: hanxuetong <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CustomLoadConfig {

	
	/**
	 * 自定义的rule处理类
	 */
	private Map<String,DecideRule> customDecideRuleMap;
	
	
	/**
	 * 添加自定义rule处理类
	 */
	public void addCustomDecideRule(String ruleName,DecideRule decideRule){
		if(customDecideRuleMap==null){
			customDecideRuleMap=new HashMap<String, DecideRule>();
		}
		if(!customDecideRuleMap.containsKey(ruleName)){
			customDecideRuleMap.put(ruleName, decideRule);
		}
		
	}


	public Map<String, DecideRule> getCustomDecideRuleMap() {
		return customDecideRuleMap;
	}
	
	
}
